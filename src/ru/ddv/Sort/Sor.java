package ru.ddv.Sort;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.Collections;
import java.util.Scanner;

public class Sor {

    static String file_path;
    static String S;

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);

        System.out.println("Введите, пожалуйста,  путь к текстовому файла:");

        file_path = s.next().replace("\\", "\\\\");

        File file = new File(file_path);
        if ( ! file.exists())
        {
            System.out.println("Файл не найден!");
            System.exit(0);
        }

        List l = readFileInList(file_path);

        Iterator<String> itr = l.iterator();
        Map map = new Hashtable();
        Integer word_count = 0;
        Integer space_count = 0;
        Integer sentence_count = 0;
        String full_text = "";

        while (itr.hasNext())
        {
            S = itr.next();
            full_text += S;

            for (char element : S.toCharArray())
                if (element == ' ') space_count++;

            String[] words = S.split("[^A-Za-zА-яа-яЁё]"); // the delimiter inside the quotes
            for(String a : words){
                if( ! a.isEmpty())
                {
                    word_count++;
                    map.put(a, (Integer)map.get(a) == null ? 1 : (Integer)map.get(a) + 1);
                }
            }
        }

        String[] sentence = full_text.split("[\\.\\!\\?]"); // the delimiter inside the quotes

        for(String a : sentence)
            if ( ! a.isEmpty())
                sentence_count++;

        System.out.println("Количество слов - " + word_count);
        System.out.println("Количество символов - " + full_text.length() + " (Не считая перенос строк)");
        System.out.println("Количество предложений - " + sentence_count);
        System.out.println("Количество пробелов  - " + space_count);

        Map<String, Integer> sortedMap = sortByValue(map);
        printMap(sortedMap);

        System.out.println("Спасибо за использование, подсчёт статистики закончен.");
    }

    public static List<String> readFileInList(String fileName) throws Exception
    {
        List<String> lines = Collections.emptyList();
        try
        {
            lines =
                    Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            System.out.println("Не могу считать UTF-8. Попробуйте другую кодировку...");
        }

        if(lines.isEmpty())
        {
            try
            {
                lines =
                        Files.readAllLines(Paths.get(fileName), Charset.forName("windows-1251"));
            }
            catch (IOException e)
            {
                System.out.println(" Извините, мы не можем работать с Вашим файлом.");
                System.exit(0);
            }
        }

        return lines;
    }

    private static Map<String, Integer> sortByValue(Map<String, Integer> unsortMap) {
        LinkedList<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>> (unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare (Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        }
        );

        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static <K, V> void printMap(Map<K, V> map) {
        System.out.println("Количество повторений слов:");

        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}